<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://rjackson.ca
 * @since      1.0.0
 *
 * @package    Wptm
 * @subpackage Wptm/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Wptm
 * @subpackage Wptm/public
 * @author     Ryan Jackson <ryan@rjackson.ca>
 */
class Wptm_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    protected $gtm_code;

    protected $dataLayer;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version     = $version;

        // $this->set_datalayer();
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Wptm_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Wptm_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/wptm-public.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Wptm_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Wptm_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/wptm-public.js', array('jquery'), $this->version, false);

        // Loop through plugins and just enable the ones I want, aka all of them! ;)
        // $this->loader->add_action('wp_head', $plugin_public, 'wp_head');
    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Wptm_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    public function create_datalayer()
    {
        $this->dataLayer = new Wptm_DataLayer();
    }

    public function gtm_primary_script()
    {
        $this->gtm_code = apply_filters('wptm_gtm_container', 'GTM-WZKH2QT');

        echo $this->dataLayer->get_script();

        echo "
	    <!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','{$this->gtm_code}');</script>
		<!-- End Google Tag Manager -->
		";

    }

    public function gtm_secondary_iframe($classes)
    {
        $script = "
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=\"{$this->gtm_code}\"
		height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		";

        $classes[] = "\">{$script}<br style=\"display:none;";

        return $classes;
    }
/*

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=' . $one_gtm_code . $_gtm_env . '"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>'
 */
    public function gtm_footer()
    {
        //       echo "
        // <!-- Google Tag Manager (noscript) -->
        // <noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=\"{$this->gtm_code}\"
        // height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
        // <!-- End Google Tag Manager (noscript) -->
        // ";
    }
}
