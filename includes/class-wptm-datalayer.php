<?php

/**
 * Fired during plugin activation
 *
 * @link       https://rjackson.ca
 * @since      1.0.0
 *
 * @package    Wptm
 * @subpackage Wptm/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wptm
 * @subpackage Wptm/includes
 * @author     Ryan Jackson <ryan@rjackson.ca>
 */
class Wptm_DataLayer
{
    private $options;

    public function __construct()
    {
        $this->options = [
            'cachable' => true,
        ];

        $this->add_datalayer_filters();
    }

    public function add_datalayer_filters()
    {
        add_filter('wptm_datalayer_data', [$this, 'get_blog_details'], 10);

        add_filter('wptm_datalayer_data', [$this, 'get_page_details'], 10);

        if (is_singular()) {
            add_filter('wptm_datalayer_data', [$this, 'get_singular_details'], 10);
        }

        if (is_archive() || is_post_type_archive()) {
            add_filter('wptm_datalayer_data', [$this, 'get_archive_details'], 10);
        }

        if (is_search()) {
            add_filter('wptm_datalayer_data', [$this, 'get_search_details'], 10);
        }

        add_filter('wptm_datalayer_data', [$this, 'get_frontpage_details'], 10);

        add_filter('wptm_datalayer_data', [$this, 'get_post_date'], 10);

        if (defined('ICL_LANGUAGE_CODE')) {
            add_filter('wptm_datalayer_data', [$this, 'get_wpml_details'], 10);
        }

        // echo '<pre>';
        // print_r($GLOBALS['wp_filter']);
        // echo '</pre>';
        // exit;

    }

    /**
     * Start the dataLayer data object
     *
     * @return array
     */
    public function get_datalayer_data()
    {
        return apply_filters('wptm_datalayer_data', ['wptm-version' => WPTM_VERSION]);
    }

    /**
     * Load the blog details into the dataLayer
     *
     * @param  array $dataLayer  dataLayer data
     * @return array
     */
    public function get_blog_details($dataLayer)
    {
        // Check to see if it's a multisite
        if (function_exists('get_blog_details')) {
            $gtm4wp_blogdetails = get_blog_details();
        } else {
            $gtm4wp_blogdetails = (object) [
                'blog_id'  => false,
                'blogname' => get_bloginfo('name'),
            ];
        }

        return array_merge($dataLayer, [
            'siteID'   => $gtm4wp_blogdetails->blog_id,
            'siteName' => $gtm4wp_blogdetails->blogname,
        ]);
    }

    /**
     * Get the visitor status
     *
     * @param  array $dataLayer  dataLayer data
     * @return array
     */
    public function get_visitor_status($dataLayer)
    {
        return array_merge($dataLayer, [
            'visitorLoginState' => \is_user_logged_in() ? "logged-in" : "logged-out",
        ]);
    }

    /**
     * Get visitor details
     *
     * @param  array $dataLayer  dataLayer data
     * @return array
     */
    public function get_visitor_details($dataLayer)
    {
        $current_user = wp_get_current_user();

        return array_merge($dataLayer, [
            "visitorType"             => $current_user->roles[0] ?? "visitor-logged-out",
            "visitorEmail"            => $current_user->user_email ?? '',
            "visitorRegistrationDate" => $current_user->user_registered ? "" : strtotime($current_user->user_registered),
            "visitorUsername"         => $current_user->user_login ?? '',
            "visitorId"               => $current_user->ID ?? '',
        ]);
    }

    /**
     * Get the page details into the dataLayer
     *
     * @param  array $dataLayer  dataLayer data
     * @return array
     */
    public function get_page_details($dataLayer)
    {
        return array_merge($dataLayer, [
            "pageTitle" => strip_tags(wp_title("|", false, "right")),
        ]);
    }

    /**
     * Get singular page details in the dataLayer
     *
     * @param  array $dataLayer  dataLayer data
     * @return array
     */
    public function get_singular_details($dataLayer)
    {
        if (!is_singular()) {
            return $dataLayer;
        }

        $post_cats    = get_the_category();
        $pageCategory = [];
        if ($post_cats) {
            foreach ($post_cats as $cat) {
                $pageCategory[] = $cat->slug;
            }
        }

        $post_tags      = get_the_tags();
        $pageAttributes = [];
        if ($post_tags) {
            foreach ($post_tags as $tag) {
                $pageAttributes[] = $tag->slug;
            }
        }

        return array_merge($dataLayer, [
            "pagePostType"   => get_post_type(),
            "pagePostType2"  => "single-" . get_post_type(),
            "pageCategory"   => $pageCategory,
            "pageAttributes" => $pageAttributes,
            "postID"         => (int) get_the_ID(),
            "postFormat"     => get_post_format() ?: 'standard',
        ]);
    }

    /**
     * Get the archive page details into the dataLayer
     *
     * @param  array $dataLayer  dataLayer data
     * @return array
     */
    public function get_archive_details($dataLayer)
    {
        foreach (['category', 'tag', 'tax', 'author', 'year', 'month', 'day', 'time', 'date'] as $type) {
            $function = "is_{$type}";

            if ($function()) {
                $archive_type = $type;
            }
        }

        return array_merge($dataLayer, [
            "pagePostType"    => get_post_type(),
            "pagePostType2"   => "{$archive_type}-" . get_post_type(),
            "postCountOnPage" => (int) $wp_query->post_count,
            "postCountTotal"  => (int) $wp_query->found_posts,
        ]);
    }

    /**
     * Get search page details into the dataLayer
     *
     * @param  array $dataLayer  dataLayer data
     * @return array
     */
    public function get_search_details($dataLayer)
    {
        return array_merge($dataLayer, [
            "siteSearchTerm"    => get_search_query(),
            "siteSearchFrom"    => $_SERVER["HTTP_REFERER"] ?? '',
            "siteSearchResults" => $wp_query->post_count,
        ]);
    }

    /**
     * Load the front page details into the dataLayer
     *
     * @param  array $dataLayer  dataLayer data
     * @return array
     */
    public function get_frontpage_details($dataLayer)
    {
        if (is_home()) {
            $pagePostType = "bloghome";
        }

        if (is_front_page()) {
            $pagePostType = "frontpage";
        }

        if (!isset($pagePostType)) {
            return $dataLayer;
        }

        return array_merge($dataLayer, [
            "pagePostType" => $pagePostType,
        ]);
    }

    /**
     * Get the author details into the dataLayer
     *
     * @param  array $dataLayer  dataLayer data
     * @return array
     */
    public function get_author_details($dataLayer)
    {
        $user = get_userdata($GLOBALS["post"]->post_author);

        if (!$user) {
            return $dataLayer;
        }

        return array_merge($dataLayer, [
            "pagePostAuthorID" => $user->ID,
            "pagePostAuthor"   => $user->display_name,
        ]);
    }

    /**
     * Load the post date into the dataLayer
     *
     * @param  array $dataLayer  dataLayer data
     * @return array
     */
    public function get_post_date($dataLayer)
    {
        // is_year, is_month, is_day, is_time, is_date
        return array_merge($dataLayer, [
            "pagePostDate"      => get_the_date(),
            "pagePostDateYear"  => get_the_date("Y"),
            "pagePostDateMonth" => get_the_date("m"),
            "pagePostDateDay"   => get_the_date("d"),
        ]);
    }

    /**
     * Load WPML data into the dataLayer
     *
     * @param  array $dataLayer  dataLayer data
     * @return array
     */
    public function get_wpml_details($dataLayer)
    {
        // Verify that the details are all here
        if (!defined('ICL_LANGUAGE_CODE') || !defined('ICL_LANGUAGE_NAME') || !defined('ICL_LANGUAGE_NAME_EN')) {
            return $dataLayer;
        }

        return array_merge($dataLayer, [
            "languageCode"        => ICL_LANGUAGE_CODE,
            "languageName"        => ICL_LANGUAGE_NAME,
            "languageEnglishName" => ICL_LANGUAGE_NAME_EN,
        ]);
    }

    /**
     * Get the script to output the dataLayer
     *
     * @return string
     */
    public function get_script()
    {
        $dataLayer = json_encode($this->get_datalayer_data(), JSON_PRETTY_PRINT);

        return "
            <script>
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({$dataLayer});
            </script>
        ";

    }
}
