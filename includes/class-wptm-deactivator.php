<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://rjackson.ca
 * @since      1.0.0
 *
 * @package    Wptm
 * @subpackage Wptm/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wptm
 * @subpackage Wptm/includes
 * @author     Ryan Jackson <ryan@rjackson.ca>
 */
class Wptm_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
