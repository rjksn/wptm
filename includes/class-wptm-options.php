<?php

/**
 * Fired during plugin activation
 *
 * @link       https://rjackson.ca
 * @since      1.0.0
 *
 * @package    Wptm
 * @subpackage Wptm/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wptm
 * @subpackage Wptm/includes
 * @author     Ryan Jackson <ryan@rjackson.ca>
 */
class Wptm_Options
{
    private $options;
}
